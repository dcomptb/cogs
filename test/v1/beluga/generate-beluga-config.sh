#!/bin/bash

cat << EOF > beluga.yml
# dcomp raven testing topology beluga config
---
listen: 172.22.1.1:5402
devices:
  n0:
    controller: raven
  n1:
    controller: raven
  n2:
    controller: raven
  n3:
    controller: raven
  n4:
    controller: raven
  n5:
    controller: raven
  n6:
    controller: raven
  n7:
    controller: raven

controllers:
  raven:
    topodir: `realpath $PWD`
EOF

