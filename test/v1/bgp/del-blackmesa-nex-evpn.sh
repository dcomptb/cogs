#!/bin/bash

set -e
set -x

mac=`ip netns exec core.lambda.blackmesa ip -j addr show dev eth1 | jq '.[2] | .address' | sed s/\"//g`

./del-evpn.sh $mac 3030 2 64705 10.99.0.5

