#!/bin/bash

set -e
set -x

mac=`ip netns exec harbor.dcomptb.io ip -j addr show dev eth1 | jq '.[2] | .address' | sed s/\"//g`

./add-evpn.sh $mac 2 2 64705 10.99.0.5

