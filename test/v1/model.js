let name = "cogs-"+Math.random().toString().substr(-6)
topo = {
  name: name,
  nodes: [
    buster('cmdr'),
    buster('stor'),
    node('n0'), node('n1'), node('n2'), node('n3'),
    node('n4'), node('n5'), node('n6'), node('n7'),
  ],
  switches: [
    cx('ileaf0'), cx('ileaf1'),
    cx('ispine'),
    cx('xleaf0'), cx('xleaf1'), cx('xleaf2'), cx('xleaf3'),
    cx('xspine'),
    cx('gw')
  ],
  links: [
    Link('cmdr', 1, 'gw', 1),


    Link('n0', 1, 'ileaf0', 1, {mac: {n0: '04:70:00:01:70:1a'}, "boot": 2}),
    Link('n0', 2, 'xleaf0', 1),
    Link('n0', 3, 'xleaf1', 1),

    Link('n1', 1, 'ileaf0', 2, {mac: {n1: '04:70:00:01:70:1B'}, "boot": 2}),
    Link('n1', 2, 'xleaf0', 2),
    Link('n1', 3, 'xleaf1', 2),

    Link('n2', 1, 'ileaf0', 3, {mac: {n2: '04:70:00:01:70:1C'}, "boot": 2}),
    Link('n2', 2, 'xleaf0', 3),
    Link('n2', 3, 'xleaf1', 3),

    Link('n3', 1, 'ileaf0', 4, {mac: {n3: '04:70:00:01:70:1D'}, "boot": 2}),
    Link('n3', 2, 'xleaf0', 4),
    Link('n3', 3, 'xleaf1', 4),

    Link('n4', 1, 'ileaf1', 1, {mac: {n4: '04:70:00:01:70:1E'}, "boot": 2}),
    Link('n4', 2, 'xleaf2', 1),
    Link('n4', 3, 'xleaf3', 1),

    Link('n5', 1, 'ileaf1', 2, {mac: {n5: '04:70:00:01:70:1F'}, "boot": 2}),
    Link('n5', 2, 'xleaf2', 2),
    Link('n5', 3, 'xleaf3', 2),

    Link('n6', 1, 'ileaf1', 3, {mac: {n6: '04:70:00:01:70:11'}, "boot": 2}),
    Link('n6', 2, 'xleaf2', 3),
    Link('n6', 3, 'xleaf3', 3),

    Link('n7', 1, 'ileaf1', 4, {mac: {n7: '04:70:00:01:70:12'}, "boot": 2}),
    Link('n7', 2, 'xleaf2', 4),
    Link('n7', 3, 'xleaf3', 4),

    Link('ileaf0', 5, 'ispine', 2),
    Link('ileaf1', 5, 'ispine', 3),

    Link('xleaf0', 5, 'xspine', 2),
    Link('xleaf1', 5, 'xspine', 3),
    Link('xleaf2', 5, 'xspine', 4),
    Link('xleaf3', 5, 'xspine', 5),

    Link('ispine', 4, 'gw', 2),

    Link('stor', 1, 'ispine', 5),
  ]
}


function node(name) {
  return {
    name: name,
    image: 'netboot',
    os: 'netboot',
    cpu: { cores: 4 },
    defaultnic: 'e1000',
    memory: { capacity: GB(4) },
    defaultdisktype: { dev: 'sda', bus: 'sata' },
    firmware: '/usr/share/ovmf/OVMF.fd',
  }
}


function buster(name) {
  return {
    name: name,
    image: 'debian-buster',
    memory: { capacity: GB(4) },
    cpu: { cores: 4 },
    mounts: [
      {source: env.PWD+'/../..', point: '/tmp/cogs'}
    ]
  }
}

function cx(name) {
  return {
    name: name,
    image: 'cumulusvx-3.7-mvrf',
  }
}

