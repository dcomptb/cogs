#!/bin/bash

set -e
set -x

if [[ $UID -ne 0 ]]; then
  echo "must be root to run"
  exit 1
fi

if [[ -z $TESTONLY ]]; then

cd keygen
./genkeys.sh
cd ..

rvn destroy
rvn build
rvn deploy
rvn pingwait cmdr ileaf0 ileaf1 xleaf0 xleaf1 xleaf2 xleaf3 xspine ispine
rvn status # to build ansible hosts file
rvn configure

cd ansible
./install-roles.sh
ansible-playbook -i ../.rvn/ansible-hosts setup.yml
ansible-playbook -i ../.rvn/ansible-hosts cog-setup.yml
ansible-playbook -i ../.rvn/ansible-hosts switch-fabric.yml
ansible-playbook -i ../.rvn/ansible-hosts stor.yml
ansible-playbook -i ../.rvn/ansible-hosts harbor-sled.yml
ansible-playbook -i ../.rvn/ansible-hosts harbor.yml
echo "waiting for harbor to come up"
sleep 20
cd ..
#./generate-beluga-config.sh
#./stop-beluga.sh
#./fetch-beluga.sh
#./start-beluga.sh

fi

if [[ $NOTEST ]]; then
  exit 0
fi

function beluga_test() {

  n0ip=`rvn ip n0`
  boottime0=`ssh -o StrictHostKeyChecking=no -i /var/rvn/ssh/rvn rvn@$n0ip "stat -c %Z /proc/"`

  #./send-mz-commands.sh

  cmdr=`rvn ip cmdr`
  ssh \
    -o StrictHostKeyChecking=no \
    -i /var/rvn/ssh/rvn \
    rvn@$cmdr \
    "sudo cog create task /tmp/cogs/test/v1/tasks/reboot-n0.yml"

  sleep 10
  rvn pingwait n0

  n0ip=`rvn ip n0` # may have changed
  boottime1=`ssh -o StrictHostKeyChecking=no -i /var/rvn/ssh/rvn rvn@$n0ip "stat -c %Z /proc/"`

  if [[ ! $boottime1 > $boottime0 ]]; then
    echo "test failed"
    echo "boottime0: $boottime0"
    echo "boottime1: $boottime1"
    exit 1
  fi

  echo "test passed"

}

function network_test() {

  cmdr=`rvn ip cmdr`
  ssh \
    -o StrictHostKeyChecking=no \
    -i /var/rvn/ssh/rvn \
    rvn@$cmdr \
    "cd /tmp/cogs/test/v1/tests && ./do-runtest.sh"

}

#beluga_test
network_test

sleep 20

rvn reboot n0 n1 n2 n3 n4 n5 n6 n7
