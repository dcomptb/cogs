package main

import (
	"gitlab.com/mergetb/xir/lang/go/hw"
	"gitlab.com/mergetb/xir/lang/go/sys"
	"gitlab.com/mergetb/xir/lang/go/tb"
)

type Testbed struct {
	Gateway *tb.Resource
	XSpine  *tb.Resource
	ISpine  *tb.Resource
	ILeaf   []*tb.Resource
	XLeaf   []*tb.Resource
	Servers []*tb.Resource
	Cables  []*hw.Cable
}

func (t *Testbed) Components() ([]*tb.Resource, []*hw.Cable) {

	rs := []*tb.Resource{t.XSpine, t.ISpine}
	rs = append(rs, t.ILeaf...)
	rs = append(rs, t.XLeaf...)
	rs = append(rs, t.Servers...)

	return rs, t.Cables

}

func main() {

	t := &Testbed{
		// testbed gateway
		Gateway: fabric(tb.Gateway, tb.Gateway, "gateway", "10,99.0.254", 2, hw.Gbps(100)),

		ISpine: fabric(tb.InfraSwitch, tb.Spine, "ispine", "10.99.0.1", 2, 100),
		XSpine: fabric(tb.XpSwitch, tb.Spine, "xspine", "10.99.0.1", 5, 100),
		ILeaf: []*tb.Resource{
			leaf(tb.InfraSwitch, "ileaf0", 5, 100),
			leaf(tb.InfraSwitch, "ileaf1", 5, 100),
		},
		XLeaf: []*tb.Resource{
			leaf(tb.XpSwitch, "xleaf0", 5, 100),
			leaf(tb.XpSwitch, "xleaf1", 5, 100),
			leaf(tb.XpSwitch, "xleaf2", 5, 100),
			leaf(tb.XpSwitch, "xleaf3", 5, 100),
		},
		Servers: []*tb.Resource{
			smallServer("n0", "04:70:00:01:70:1A"),
			smallServer("n1", "04:70:00:01:70:1B"),
			mediumServer("n2", "04:70:00:01:70:1C"),
			largeServer("n3", "04:70:00:01:70:1D"),
			smallServer("n4", "04:70:00:01:70:1E"),
			smallServer("n5", "04:70:00:01:70:1F"),
			mediumServer("n6", "04:70:00:01:70:11"),
			largeServer("n7", "04:70:00:01:70:12"),
		},
	}

	// xspine - gateway
	t.Cable().Connect(swp(t.XSpine, 4), swp(t.Gateway, 0))

	// ileaf -ixspine
	t.Cable().Connect(swp(t.ISpine, 0), swp(t.ILeaf[0], 4))
	t.Cable().Connect(swp(t.ISpine, 1), swp(t.ILeaf[1], 4))

	// xleaf - xspine
	t.Cable().Connect(swp(t.XSpine, 0), swp(t.XLeaf[0], 4))
	t.Cable().Connect(swp(t.XSpine, 1), swp(t.XLeaf[1], 4))
	t.Cable().Connect(swp(t.XSpine, 2), swp(t.XLeaf[2], 4))
	t.Cable().Connect(swp(t.XSpine, 3), swp(t.XLeaf[3], 4))

	// server - ileaf
	t.Cable().Connect(eth(t.Servers[0], 1), swp(t.ILeaf[0], 0))
	t.Cable().Connect(eth(t.Servers[1], 1), swp(t.ILeaf[0], 1))
	t.Cable().Connect(eth(t.Servers[2], 1), swp(t.ILeaf[0], 2))
	t.Cable().Connect(eth(t.Servers[3], 1), swp(t.ILeaf[0], 3))

	t.Cable().Connect(eth(t.Servers[4], 1), swp(t.ILeaf[1], 0))
	t.Cable().Connect(eth(t.Servers[5], 1), swp(t.ILeaf[1], 1))
	t.Cable().Connect(eth(t.Servers[6], 1), swp(t.ILeaf[1], 2))
	t.Cable().Connect(eth(t.Servers[7], 1), swp(t.ILeaf[1], 3))

	// server - xleaf
	t.Cable().Connect(eth(t.Servers[0], 2), swp(t.XLeaf[0], 0))
	t.Cable().Connect(eth(t.Servers[0], 3), swp(t.XLeaf[1], 0))
	t.Cable().Connect(eth(t.Servers[1], 2), swp(t.XLeaf[0], 1))
	t.Cable().Connect(eth(t.Servers[1], 3), swp(t.XLeaf[1], 1))
	t.Cable().Connect(eth(t.Servers[2], 2), swp(t.XLeaf[0], 2))
	t.Cable().Connect(eth(t.Servers[2], 3), swp(t.XLeaf[1], 2))
	t.Cable().Connect(eth(t.Servers[3], 2), swp(t.XLeaf[0], 3))
	t.Cable().Connect(eth(t.Servers[3], 3), swp(t.XLeaf[1], 3))

	t.Cable().Connect(eth(t.Servers[4], 2), swp(t.XLeaf[2], 0))
	t.Cable().Connect(eth(t.Servers[4], 3), swp(t.XLeaf[3], 0))
	t.Cable().Connect(eth(t.Servers[5], 2), swp(t.XLeaf[2], 1))
	t.Cable().Connect(eth(t.Servers[5], 3), swp(t.XLeaf[3], 1))
	t.Cable().Connect(eth(t.Servers[6], 2), swp(t.XLeaf[2], 2))
	t.Cable().Connect(eth(t.Servers[6], 3), swp(t.XLeaf[3], 2))
	t.Cable().Connect(eth(t.Servers[7], 2), swp(t.XLeaf[2], 3))
	t.Cable().Connect(eth(t.Servers[7], 3), swp(t.XLeaf[3], 3))

	tbxir := tb.ToXir(t)
	tbxir.Table()

	tbxir.ToFile("tbxir.json")

}

func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(100))
	t.Cables = append(t.Cables, cable)
	return cable

}

func fabric(kind, role tb.Role, name, ip string, radix int, gbps uint64) *tb.Resource {

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.NetAlloc},
		Roles:  []tb.Role{kind, role},
		System: sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
	}
	rs.System.OS.Config = sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: ip,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	rs.System.Device.Model = "Generic Spine Switch"

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
	}

	return rs

}

func leaf(kind tb.Role, name string, radix int, gbps uint64) *tb.Resource {

	rs := &tb.Resource{
		Roles:  []tb.Role{kind, tb.Leaf},
		System: sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
	}

	rs.System.Device.Model = "Generic Leaf Switch"
	rs.System.OS.Config = sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
	}

	return rs

}

func smallServer(name, mac string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
	}
	s.Nics[0].Ports[1].Mac = mac

	system := sys.NewSystem(name, s)
	system.Props["append"] = "root=/dev/sda1 rootfstype=ext4 rw net.ifnames=0 biosdevname=0"
	system.Props["rootdev"] = "sda"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Small Server"

	return rs

}

func mediumServer(name, mac string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
		{Cores: 8},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
	}
	s.Nics[0].Ports[1].Mac = mac

	system := sys.NewSystem(name, s)
	system.Props["append"] = "root=/dev/sda1 rootfstype=ext4 rw net.ifnames=0 biosdevname=0"
	system.Props["rootdev"] = "sda"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Medium Server"

	return rs

}

func largeServer(name, mac string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 16},
		{Cores: 16},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
	}
	s.Nics[0].Ports[1].Mac = mac

	system := sys.NewSystem(name, s)
	system.Props["append"] = "root=/dev/sda1 rootfstype=ext4 rw net.ifnames=0 biosdevname=0"
	system.Props["rootdev"] = "sda"

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		System: system,
	}

	rs.System.Device.Model = "Large Server"

	return rs

}

func nics(r *tb.Resource) []hw.Nic {
	return r.System.Device.Nics
}

func swp(r *tb.Resource, i int) *hw.Port {
	return nics(r)[1].Ports[i]
}

func eth(r *tb.Resource, i int) *hw.Port {
	return nics(r)[0].Ports[i]
}
