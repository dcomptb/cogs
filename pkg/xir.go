package cogs

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/lang/go"
	"gitlab.com/mergetb/xir/lang/go/tb"
)

const (
	XIR_FILE_PATH = "/etc/dcomp/tb-xir.json"
)

func LoadXIR() (*xir.Net, error) {
	return xir.FromFile(XIR_FILE_PATH)
}

func GetInfraEndpoint(n *xir.Node) *xir.Endpoint {

	for _, e := range n.Endpoints {
		for _, nbr := range e.Neighbors {
			if tb.HasRole(nbr.Endpoint.Parent, tb.Leaf) &&
				tb.HasRole(nbr.Endpoint.Parent, tb.InfraSwitch) {

				return e

			}
		}
	}

	return nil

}

func Merge2Local(devId string) (string, error) {

	//TODO(ry) may want to cache this with an FS watcher for updates?
	net, err := LoadXIR()
	if err != nil {
		log.WithError(err).Error("failed to load xir")
		return "", err
	}

	_, _, node := net.GetNode(devId)
	if node == nil {
		log.WithFields(log.Fields{
			"uuid": devId,
		}).Error("could not find node")
		return "", fmt.Errorf("node not found")
	}

	return node.Label(), nil

}

func GetNodeLeaf(nodeEndpoint *xir.Endpoint) *xir.Endpoint {

	for _, n := range nodeEndpoint.Neighbors {
		if tb.HasRole(n.Endpoint.Parent, tb.Leaf) {
			return n.Endpoint
		}
	}

	return nil

}

func ExtractConstraint(props map[string]interface{}, key string) *xir.Constraint {

	data, ok := props[key]
	if !ok {
		return nil
	}

	constraint := &xir.Constraint{}
	err := mapstructure.Decode(data, constraint)
	if err == nil {
		return nil
	}

	return constraint

}

func AllResourceIds(net *xir.Net) []string {

	var ids []string
	for _, x := range net.Nodes {
		ids = append(ids, x.Id)
	}
	for _, x := range net.Links {
		ids = append(ids, x.Id)
	}

	for _, x := range net.Nets {
		ids = append(ids, AllResourceIds(x)...)
	}

	return ids

}
