package cogs

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/sled/pkg"
)

const (
	defaultImage = "debian:buster"
)

func userImageData(model map[string]interface{}) (*ImageBinding, error) {

	imageConstraint := ExtractConstraint(model, "image")
	if imageConstraint == nil {
		return imageSpecFromName(defaultImage)
	}
	imageValue, ok := imageConstraint.Value.(string)
	if !ok {
		return imageSpecFromName(defaultImage)
	}

	return imageSpecFromName(imageValue)

}

func imageSpecFromName(name string) (*ImageBinding, error) {

	parts := strings.Split(name, ":")
	if len(parts) != 2 {
		return nil, common.ErrorF(
			"invalid image name, must be <name>:<version>", log.Fields{
				"name": name,
			})
	}

	os, version := parts[0], parts[1]

	return &ImageBinding{
		Image: &sled.Image{
			Name:      name,
			Kernel:    fmt.Sprintf("%s-%s-kernel", os, version),
			Initramfs: fmt.Sprintf("%s-%s-initramfs", os, version),
			Disk:      fmt.Sprintf("%s-%s-disk", os, version),
		},
	}, nil

}

type ImageDefaults struct {
	Image sled.Image
	ver   int64
}

func (d *ImageDefaults) Key() string        { return "/defaults/image" }
func (d *ImageDefaults) GetVersion() int64  { return d.ver }
func (d *ImageDefaults) SetVersion(v int64) { d.ver = v }
func (d *ImageDefaults) Value() interface{} { return d }

var imageDefaults *ImageDefaults

func loadImageDefaults() {

	d := &ImageDefaults{}
	err := Read(d)
	if err != nil {
		log.WithError(err).Fatal("failed to read image defaults")
	}

}
