package cogs

import (
	"flag"
)

func Init() {

	flag.Parse()

	initContainers()
	initBgp()

}
