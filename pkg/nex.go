package cogs

import (
	"gitlab.com/mergetb/tech/nex/pkg"
)

const (
	NexNamespace   = "nex"
	NexImage       = "docker.io/mergetb/nex:latest"
	NexMountName   = "nex-config"
	NexMountSource = "/etc/cogs/nex.yml"
	NexMountDest   = "/etc/nex/nex.yml"
	NexDomainEnv   = "DOMAIN"
)

type NexNetworkSpec struct {
	// Core Nex data
	Network nex.Network

	// Nex endpoint address
	Endpoint string
}

type NexMemberSpec struct {
	nex.MemberList `mapstructure:",squash" yaml:",inline"`
	Endpoint       string
}
