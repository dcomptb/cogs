package cogs

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
)

const (
	timeout = 5 * time.Second
)

type CogletInfo struct {
	Id   string
	Cog  string
	Host string
	Pid  int

	version int64
}

// storage object implementation
func (c *CogletInfo) Key() string        { return "/coglet/" + c.Id }
func (c *CogletInfo) GetVersion() int64  { return c.version }
func (c *CogletInfo) SetVersion(v int64) { c.version = v }
func (c *CogletInfo) Value() interface{} { return c }

func RegisterCoglet(c *etcd.Client, cog string) (error, etcd.LeaseID) {

	host, err := os.Hostname()
	if err != nil {
		return err, -1
	}

	ci := &CogletInfo{
		Id:   clid,
		Cog:  cog,
		Host: host,
		Pid:  os.Getpid(),
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	lease, err := c.Grant(ctx, 5)
	defer cancel()
	if err != nil {
		return err, -1
	}

	return Write(ci, etcd.WithLease(lease.ID)), lease.ID

}

func ListCoglets() ([]*CogletInfo, error) {

	var infos []*CogletInfo
	err := withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := c.Get(ctx, "/coglet", etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			id, err := extractCogletId(string(kv.Key))
			if err != nil {
				return err
			}
			ci := &CogletInfo{Id: id}
			err = Read(ci)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"id": id,
				}).Warn("failed to read coglet")
				continue
			}
			infos = append(infos, ci)

		}

		return nil

	})
	if err != nil {
		return nil, err
	}

	return infos, nil
}

func extractCogletId(key string) (string, error) {
	parts := strings.Split(key, "/")
	if len(parts) != 3 {
		return "", fmt.Errorf("malformed coglet key '%s'", key)
	}
	return parts[2], nil
}
