package cogs

import (
	"context"
	"encoding/json"
	"flag"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/dcomptb/cogs/common"
	cm "gitlab.com/mergetb/site/api"
)

var (
	certFile = flag.String("cmdr-cert", "/etc/merge/cmdr.pem", "commander cert file")
)

func HandleIncoming(rq *cm.IncomingRequest) error {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	// extract properties payload
	props := make(map[string]interface{})
	err := json.Unmarshal([]byte(rq.Data), &props)
	if err != nil {
		log.WithFields(fields).WithError(err).Warn(
			"bad or incomplete  incomming data")
	}

	switch rq.Code {

	// setup infranet
	case cm.IncomingRequest_Setup:
		log.WithFields(fields).Info("incoming setup")

		return InitInfranet(rq.Mzid, rq.Instance, props, true)

	// tear down infranet
	case cm.IncomingRequest_Teardown:
		log.WithFields(fields).Info("incoming teardown")

		return TeardownInfranet(rq.Mzid, rq.Instance, props, true)

	}

	return common.ErrorF("unknown incoming code", fields)
}

func HandleStatus(rq *cm.StatusRequest) (*cm.StatusResponse, error) {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Info("status request")

	return status(rq.Mzid, rq.Instance, rq)

}

func HandleNodeSetup(rq *cm.MzRequest) error {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Info("node setup")

	return NodeSetup(rq.Mzid, rq.Instance, rq.Fragments)

}

func HandleLinkCreate(rq *cm.MzRequest) error {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Info("link create")

	return CreateLinks(rq.Mzid, rq.Instance, rq.Fragments)

}

func HandleNodeRecycle(rq *cm.MzRequest) error {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Info("node recycle")

	return NodeRecycle(rq.Mzid, rq.Instance, rq.Fragments)

}

func HandleLinkDestroy(rq *cm.MzRequest) error {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Info("link destroy")

	return DestroyLinks(rq.Mzid, rq.Instance, rq.Fragments)

}

func RegisterWithCommander() {

	xir, err := LoadXIR()
	if err != nil {
		log.WithError(err).Fatal("failed to load site xir")
	}
	devices := AllResourceIds(xir)

	cfg := common.GetConfig()

	Cmdr(cfg, func(cc cm.CommanderClient) error {

		register := &cm.RegisterRequest{
			Host:   cfg.Driver.Address,
			Port:   int32(cfg.Driver.Port),
			Driver: "dcomp-driver",
			Ids:    devices,
		}

		ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
		defer cancel()
		_, err = cc.Register(ctx, register)
		if err != nil {
			log.WithError(err).Fatal("failed to connect to commander")
		}

		return nil

	})
}

func CommanderClient(cfg common.Config) (
	*grpc.ClientConn, cm.CommanderClient, error) {

	creds, err := credentials.NewClientTLSFromFile(*certFile, "")
	if err != nil {
		return nil, nil, err
	}

	conn, err := grpc.Dial(
		cfg.Commander.Endpoint(),
		grpc.WithTransportCredentials(creds),
	)
	if err != nil {
		return nil, nil, common.ErrorEF("could not connect to commander service",
			err,
			log.Fields{"addr": cfg.Commander.Endpoint()},
		)
	}
	return conn, cm.NewCommanderClient(conn), nil
}

func Cmdr(cfg common.Config, f func(cm.CommanderClient) error) error {

	conn, ccc, err := CommanderClient(cfg)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(ccc)

}
