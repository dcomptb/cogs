package cogs

import (
	"context"
	"flag"
	"fmt"
	"net"
	"time"

	api "github.com/osrg/gobgp/api"
	"github.com/osrg/gobgp/pkg/packet/bgp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"google.golang.org/grpc"

	. "gitlab.com/dcomptb/cogs/common"
)

// user flags
var (
	GobgpdPort = flag.Int("gobgpd-port", 50051, "gobgpd listening port")
	BgpAddr    = flag.String("bgp-addr", "", "bgp address")
	BgpAS      = flag.Int("bgp-as", 0, "bgp autonomus system number")
)

type EvpnWithdrawData struct {
	Vindex int `yaml:",omitempty"`
	Vni    int `yaml:",omitempty"`
}

func initBgp() {

	if *BgpAddr == "" {
		log.Fatal("must supply bgp address")
	}

	if *BgpAS == 0 {
		log.Fatal("must supply bgp AS number")
	}

	// get the address being advertised
	ip, mask, err := net.ParseCIDR(*BgpAddr)
	if ip == nil {
		log.
			WithFields(log.Fields{"address": *BgpAddr}).
			Fatal("bgp address is invalid, expecting cidr e.g. 1.2.3.4/24")
	}

	// get the address subnet
	ones, _ := mask.Mask.Size()
	nlri := bgp.NewIPAddrPrefix(uint8(ones), ip.String())

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop("0.0.0.0"),
	}

	// send msg to gobgp
	err = withGoBgp(func(client api.GobgpApiClient) error {

		_, err := client.AddPath(context.Background(), &api.AddPathRequest{
			TableType: api.TableType_GLOBAL,
			Path:      NewPath(nlri, false, attrs, time.Now()),
		})

		return err

	})
	if err != nil {
		log.WithError(err).Fatal("failed to init bgp")
	}

}

func EvpnAdvertiseMac(veth *rtnl.Link, vni, vindex int) error {
	return evpnUpdateMac(veth, vni, vindex, false)
}
func EvpnWithdrawMac(veth *rtnl.Link, vni, vindex int) error {
	return evpnUpdateMac(veth, vni, vindex, true)
}

func evpnUpdateMac(veth *rtnl.Link, vni, vindex int, withdraw bool) error {

	info, err := evpnAdvertiseInfo(vni, vindex)
	if err != nil {
		return err
	}

	r := &bgp.EVPNMacIPAdvertisementRoute{
		RD:               info.Rd,
		MacAddressLength: 48,
		MacAddress:       veth.Info.Address,
		IPAddressLength:  uint8(net.IPv4len * 8),
		IPAddress:        nil,
		Labels:           []uint32{uint32(vni)},
		ETag:             uint32(0),
	}

	nlri := bgp.NewEVPNNLRI(bgp.EVPN_ROUTE_TYPE_MAC_IP_ADVERTISEMENT, r)

	extcomms := []bgp.ExtendedCommunityInterface{
		bgp.NewEncapExtended(bgp.TUNNEL_TYPE_VXLAN),
		info.Rt,
	}

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop(info.BgpIP.String()),
		bgp.NewPathAttributeExtendedCommunities(extcomms),
	}

	if withdraw {
		return evpnDoWithdraw(nlri, attrs)
	}
	return evpnDoAdvertise(nlri, attrs)

}

type EvpnInfo struct {
	BgpIP net.IP
	Rd    bgp.RouteDistinguisherInterface
	Rt    bgp.ExtendedCommunityInterface
}

func evpnAdvertiseInfo(vni, vindex int) (*EvpnInfo, error) {

	bip, _, err := net.ParseCIDR(*BgpAddr)
	if err != nil {
		return nil, ErrorEF("failed to parse bgp-ip", err, log.Fields{"ip": *BgpAddr})
	}

	rd, err := bgp.ParseRouteDistinguisher(
		fmt.Sprintf("%s:%d", bip.String(), vindex),
	)
	if err != nil {
		return nil, ErrorE("failed to parse route distinguisher", err)
	}

	rt, err := bgp.ParseRouteTarget(fmt.Sprintf("%d:%d", *BgpAS, vni))
	if err != nil {
		return nil, ErrorE("failed to parse route target", err)
	}

	return &EvpnInfo{
		BgpIP: bip,
		Rd:    rd,
		Rt:    rt,
	}, nil

}

func evpnDoAdvertise(nlri bgp.AddrPrefixInterface, attrs []bgp.PathAttributeInterface) error {

	return withGoBgp(func(client api.GobgpApiClient) error {

		_, err := client.AddPath(context.Background(), &api.AddPathRequest{
			TableType: api.TableType_GLOBAL,
			VrfId:     "",
			Path:      NewPath(nlri, false, attrs, time.Now()),
		})

		if err != nil {
			return ErrorE("failed to add evpn multicast path", err)
		}
		return nil

	})

}

func evpnDoWithdraw(nlri bgp.AddrPrefixInterface, attrs []bgp.PathAttributeInterface) error {

	return withGoBgp(func(client api.GobgpApiClient) error {

		_, err := client.DeletePath(context.Background(), &api.DeletePathRequest{
			TableType: api.TableType_GLOBAL,
			Family: &api.Family{
				Afi:  api.Family_AFI_L2VPN,
				Safi: api.Family_SAFI_EVPN,
			},
			Path: NewPath(nlri, false, attrs, time.Now()),
		})

		if err != nil {
			return ErrorE("failed to add evpn multicast path", err)
		}
		return nil

	})

}

func EvpnAdvertiseMulticast(vni, vindex int) error { return evpnUpdateMulticast(vni, vindex, false) }
func EvpnWithdrawMulticast(vni, vindex int) error  { return evpnUpdateMulticast(vni, vindex, true) }

func evpnUpdateMulticast(vni, vindex int, withdraw bool) error {

	info, err := evpnAdvertiseInfo(vni, vindex)
	if err != nil {
		return err
	}

	r := &bgp.EVPNMulticastEthernetTagRoute{
		RD:              info.Rd,
		IPAddressLength: uint8(net.IPv4len * 8),
		IPAddress:       info.BgpIP,
		ETag:            uint32(0),
	}

	nlri := bgp.NewEVPNNLRI(bgp.EVPN_INCLUSIVE_MULTICAST_ETHERNET_TAG, r)

	extcomms := []bgp.ExtendedCommunityInterface{
		bgp.NewEncapExtended(bgp.TUNNEL_TYPE_VXLAN),
		info.Rt,
	}

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop(info.BgpIP.String()),
		bgp.NewPathAttributeExtendedCommunities(extcomms),
	}

	if withdraw {
		return evpnDoWithdraw(nlri, attrs)
	}
	return evpnDoAdvertise(nlri, attrs)

}

func withGoBgp(f func(api.GobgpApiClient) error) error {

	endpoint := fmt.Sprintf("localhost:%d", *GobgpdPort)
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("failed to connect to GoBGP")
		return err
	}
	defer conn.Close()

	client := api.NewGobgpApiClient(conn)
	return f(client)
}
