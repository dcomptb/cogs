package cogs

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"

	. "gitlab.com/dcomptb/cogs/common"
)

var Version = "undefined"

var (
	AlreadyWorking error = fmt.Errorf("cog already running")
	clid           string
)

func init() {
	id, _ := uuid.NewV4()
	clid = id.String()
}

func CogletId() string {
	return clid
}

// Exec is the primary lower half subroutine. It is designed to be executed in
// an infinite loop by lower half implementations.
func Exec(etcdp **clientv3.Client, cog string, doTask func(*Task) error) error {

	cfg := GetConfig()
	err := EnsureEtcd(etcdp)
	if err != nil {
		return err
	}

	// now that the db connection is validated, actually try and do some work
	err = CheckForTasks(*etcdp, doTask)
	if err != nil {
		err = ErrorE("task error", err)
		time.Sleep(time.Duration(cfg.Tuning.FailSleep) * time.Second)
		return err
	}

	// sleep for the configured task quantum before the next loop
	time.Sleep(time.Duration(cfg.Tuning.TaskQuantum) * time.Millisecond)
	return nil

}

func CheckForTasks(
	etcd *clientv3.Client,
	doTask func(*Task) error,
) error {

	//fetch the task list for this cog
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	tasks, err := etcd.Get(ctx, "/task/", clientv3.WithPrefix())
	cancel()
	if err != nil {
		return ErrorE("error fetching cog tasks", err)
	}
	for _, x := range tasks.Kvs {

		t := &Task{}
		err := json.Unmarshal(x.Value, t)
		if err != nil {
			return err
		}
		t.version = x.Version

		if t.Complete() || t.Failed() != nil {
			continue
		}

		if !t.DepsSatisfied() {
			continue
		}

		// lease the task
		lease, err := LeaseTask(etcd, t)
		if err != nil {
			return err
		}
		if lease == 0 { // lease taken
			continue
		}

		fields := log.Fields{"id": short(t.Id)}

		log.WithFields(fields).Trace("check")

		// TODO(ry): deciding *not* to garbage collect tasks for now. In large part
		// this is to support the task.Wait() functionality, so clients can wait for
		// a task to complete and check the result. It's likeley that the usefulness
		// of a completed task record diminishes proportional to its age. But what
		// that limit should be set to is unclear. For example, completed task
		// records could be very useful for debugging a sidways materialization.
		//
		// Another reason we need to keep tasks around is to support one task
		// waiting on another. If we kill the tasks right after they complete, or
		// some short time after, it's impossible to guarantee that a follow-on task
		// can distinguish between a completed task and a never launched task.
		//
		// TODO(ry): what is clear is that we should provide a way in the cog utilty
		// for an administrator to manually clean up completed task records.
		//
		// garbage collection, if we have the lease and the task is done, it's ok
		// to delete it
		/*
			if t.Complete() {
				log.WithFields(fields).Info("garbage collection")
				etcd.Delete(context.TODO(), t.Key())
				RevokeLease(etcd, lease)
				continue
			}
		*/

		// the task is not complete and we have the lease, carry out the task
		t.Coglet = clid
		Write(t)
		HandleTask(etcd, t, lease, doTask)

	}

	return nil

}

func RevokeLease(etcd *clientv3.Client, lease clientv3.LeaseID) {
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()
	_, err := etcd.Revoke(ctx, lease)
	if err != nil {
		log.WithError(err).Warn("error revoking task lease")
	}
}

func KeepLeaseAlive(etcd *clientv3.Client, lease clientv3.LeaseID) (
	chan bool, error) {

	ka, err := etcd.KeepAlive(context.TODO(), lease)
	if err != nil {
		return nil, err
	}

	done := make(chan bool)
	go func(ka <-chan *clientv3.LeaseKeepAliveResponse, done chan bool) {
		for {
			select {
			case <-done:
				return
			case <-ka:
				log.WithFields(log.Fields{"lease": lease}).Trace("lease keepalive")
			default:
			}
		}
	}(ka, done)

	return done, nil

}
