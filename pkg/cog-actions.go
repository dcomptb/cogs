package cogs

import (
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/sled/pkg"
)

const (
	HarborNet      = "harbor.dcomptb.net"
	HarborMzid     = "harbor.dcomptb"
	HarborSvcAddr  = "172.31.0.2" //XXX hardcode
	HarborVindex   = 2
	HarborInfraVNI = 2
	HarborInfraVID = 2
	HarborXpVNI    = 2
)

const (

	// container ...................................

	ContainerKind   = "container"
	LaunchCtr       = "launch"
	PullCtrImage    = "pull-image"
	CreateCtr       = "create"
	CreateCtrTask   = "create-task"
	CreateCtrNs     = "create-ns"
	ConfigCtrLo     = "config-lo"
	CreateCtrRecord = "create-record"
	DeleteCtr       = "delete"
	DeleteCtrTask   = "delete-task"
	DeleteCtrRecord = "delete-record"
	DeleteNS        = "delete-ns"

	// node

	NodeSetupKind   = "NodeSetup"
	NodeRecycleKind = "NodeRecycle"

	// network plumbing ............................

	PlumbingKind   = "plumbing"
	CreateEnclave  = "create-enclave"
	DestroyEnclave = "destroy-enclave"
	AdvertiseMac   = "advertise-mac"

	// nex .........................................

	NexKind          = "Nex"
	CreateNexNetwork = "CreateNetwork"
	DeleteNexNetwork = "DeleteNetwork"
	DeleteNexMembers = "DeleteMembers"
	AddNexMembers    = "AddMembers"

	// canopy ......................................

	CanopyKind              = "Canopy"
	SetCanopyLinks          = "SetLinks"
	SetCanopyServiceVtep    = "SetServiceVtep"
	RemoveCanopyServiceVtep = "RemoveServiceVtep"
	RemoveCanopyLinks       = "RemoveLinks"

	// bookkeeping .................................

	BookkeepingKind = "bookkeeping"
	DeleteMzinfo    = "DeleteMzinfo"

	// the following are scaffolding only

	OtterKind = "Otter"
	SetOtter  = "Set"

	NimbusKind         = "Nimbus"
	DeleteNimbusConfig = "Delete"
	CreateNimbusConfig = "Create"

	RallyKind    = "Rally"
	RecycleRally = "Recycle"
	EnsureRally  = "Ensure"
)

const (
	//assumes rex configures the machine he's running on
	InfraCanopyHost = "localhost"
	Svcbr           = "svcbr"
)

//// Nex //////////////////////////////////////////////////////////////////////

func NexDeleteNetwork(name string) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   name,
		Action: DeleteNexNetwork,
		Data: nex.Network{
			Name: name,
		},
	}

}

func NexDeleteMembers(mzid string, endpoints []Endpoint) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   mzid,
		Action: DeleteNexMembers,
		Data:   endpoints,
	}

}

func NexCreateMembers(mzid string, endpoints []Endpoint) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   mzid,
		Action: AddNexMembers,
		Data:   endpoints,
	}

}

//// Container ////////////////////////////////////////////////////////////////

type ContainerImageData struct {
	Image     string
	Namespace string
}

type DeleteContainerData struct {
	Name      string
	Namespace string
}

type DeleteContainerIfxData struct {
	Name         string
	EvpnWithdraw bool
}

//// Canopy ///////////////////////////////////////////////////////////////////

type LinkKind string

const (
	InfraLink LinkKind = "infra"
	XpLink    LinkKind = "xp"
)

type VLinkEdge int

const (
	VlanAccess VLinkEdge = iota
	VlanTrunk
	Vxlan
)

type Endpoint struct {
	Node        string
	Interface   string
	Multidegree bool
}

type LinkSpec struct {
	Kind      LinkKind
	Vni       int
	Vid       int
	Links     []string
	Endpoints []Endpoint
}

type BelugaSpec struct {
	Soft  bool
	Nodes []string
}

type Vtep struct {
	Node     string
	Dev      string
	Bridge   string
	TunnelIP string
}

type VtepSpec struct {
	Vni   int
	Vid   int
	Vteps []Vtep
}

//// node spec ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

type NodeSpec struct {
	Resource    string
	Xname       string
	Mac         string
	Interface   string
	Multidegree bool
	SvcEndpoint string
	Infranet    *TargetNetwork
	Binding     *ImageBinding
	Config      *foundry.MachineConfig
}

type TargetNetwork struct {
	Vni int
	Vid int
}

//// sled /////////////////////////////////////////////////////////////////////

type ImageBinding struct {
	//Node  string
	//Mac   string
	Image *sled.Image // image storage information
	Kexec *sled.Kexec // local kexec after initial sled write
}

type ImageSpec struct {
	Endpoint string // location of sledb
	List     []ImageBinding
	NoVerify bool
}

//// foundry //////////////////////////////////////////////////////////////////

type FoundrySetSpec struct {
	Endpoint string
	Machines []*foundry.MachineConfig
}

//TODO
//TODO ========================================================================
//TODO

//// Otter

type OtterSpec struct {
	Pubkeys   []string
	Endpoints []Endpoint
}

//// Nimbus

type NimbusUser struct {
	Name              string
	Sudo              bool
	SshAuthorizedKeys []string
}

type NimbusConfig struct {
	Node  string
	Users []NimbusUser
}

//// Rally

type RallyStore struct {
	Id string
}
