package cogs

import (
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/site/api"
)

func status(mzid, instance string, rq *site.StatusRequest) (*site.StatusResponse, error) {

	result := &site.StatusResponse{}

	tasks, err := ListTasks()
	if err != nil {
		return nil, common.ErrorE("failed to list tasks", err)
	}

	for _, t := range tasks {
		for _, s := range t.Stages {
			for _, a := range s.Actions {

				if a.Mzid != mzid {
					continue
				}

				if a.MzInstance != instance {
					continue
				}

				if a == nil {
					continue
				}

				errmsg := ""
				if a.Error != nil {
					errmsg = *a.Error
				}

				result.Tasks = append(result.Tasks, &site.Task{
					Kind:     a.Kind,
					Name:     a.Action,
					Error:    errmsg,
					Complete: a.Complete,
				})

			}
		}
	}

	return result, nil

}
