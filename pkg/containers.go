package cogs

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"runtime"
	"strings"
	"time"

	. "gitlab.com/dcomptb/cogs/common"

	"github.com/containerd/containerd"
	"github.com/containerd/containerd/cio"
	"github.com/containerd/containerd/containers"
	"github.com/containerd/containerd/namespaces"
	"github.com/containerd/containerd/oci"
	"github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"
	"syscall"
)

const (
	DefaultMountType = "bind"
)

var (
	DefaultMountOptions = []string{"rbind", "ro"}
)

// Container Query ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func ListContainers(namespace string) ([]containerd.Container, error) {

	client, err := containerdClient()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	ctx := namespaces.WithNamespace(context.Background(), namespace)
	return client.Containers(ctx)

}

func ListNamespaces() ([]string, error) {

	client, err := containerdClient()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	return client.NamespaceService().List(context.TODO())

}

// Container Creation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func SetupContainerLo(namespace string) error {

	ctx, err := rtnl.OpenContext(namespace)
	if err != nil {
		return ErrorE("open context", err)
	}
	defer ctx.Close()

	return setupLo(ctx)

}

func PullContainerImage(namespace, image string) error {

	client, err := containerdClient()
	if err != nil {
		return ErrorE("containerd client", err)
	}

	ctx := namespaces.WithNamespace(context.Background(), namespace)
	_, err = client.Pull(ctx, image, containerd.WithPullUnpack)
	if err != nil {
		return ErrorE("pull image", err)
	}

	return nil

}

func CreateContainer(ctr *ContainerSpec) error {

	client, err := containerdClient()
	if err != nil {
		return ErrorE("containerd client", err)
	}

	fields := log.Fields{
		"ctrNs": ctr.Namespace,
		"image": ctr.Image,
	}

	ctx := namespaces.WithNamespace(context.Background(), ctr.Namespace)

	img, err := client.GetImage(ctx, ctr.Image)
	if err != nil {
		return ErrorEF("get image", err, fields)
	}

	sopts := []oci.SpecOpts{
		oci.WithImageConfig(img),
		oci.WithEnv(ctr.Env()),
		WithCogCNI(ctr.Namespace),
		WithCtrMounts(ctr.Mounts),
	}

	// create container
	_, err = client.NewContainer(
		ctx,
		ctr.Name,
		containerd.WithImage(img),
		containerd.WithNewSnapshot(fmt.Sprintf("%s-snapshot", ctr.Name), img),
		containerd.WithNewSpec(sopts...),
	)
	if err != nil {
		if strings.Contains(err.Error(), "already exists") {
			return nil
		}
		return ErrorE("new container", err)
	}

	return nil

}

func CreateContainerTask(ctr *ContainerSpec) error {

	client, err := containerdClient()
	if err != nil {
		return ErrorE("containerd client", err)
	}

	ctx := namespaces.WithNamespace(context.Background(), ctr.Namespace)

	container, err := client.LoadContainer(ctx, ctr.Name)
	if err != nil {
		return ErrorE("load container", err)
	}

	task, err := container.Task(ctx, nil)
	if err != nil || task == nil {

		// ensure container logfile directory exists
		cnsDir := fmt.Sprintf("/var/log/containers/%s", ctr.Namespace)
		err = os.MkdirAll(cnsDir, 0755)
		if err != nil {
			return ErrorE("create container log dir", err)
		}

		// create a task object for the container
		logfile := fmt.Sprintf("%s/%s", cnsDir, ctr.Name)
		task, err = container.NewTask(ctx, cio.LogFile(logfile))
		if err != nil {
			return ErrorE("new ctr task", err)
		}

		// start the container task
		err = task.Start(ctx)
		if err != nil {
			return ErrorE("failed to start ctr task", err)
		}

		// wait for task to actually start
		err = waitForProcessStart(ctx, task)
		if err != nil {
			return ErrorE("ctr task did not start", err)
		}

	}

	return nil

}

func waitForProcessStart(ctx context.Context, task containerd.Task) error {

	for i := 0; i < 10; i++ {

		s, err := task.Status(ctx)
		if err != nil {
			return ErrorE("failed to get ctr task status", err)
		}

		if s.Status == containerd.Running {
			return nil
		}

		time.Sleep(250 * time.Millisecond)

	}

	return Error("ctr task failed to start")

}

func WithCtrMounts(mounts []*ContainerMount) oci.SpecOpts {

	return func(
		ctx context.Context,
		client oci.Client,
		c *containers.Container,
		spec *oci.Spec,
	) error {

		for _, mount := range mounts {
			spec.Mounts = append(spec.Mounts, mount.Mount)
		}
		return nil

	}

}

func CreateContainerRecord(spec *ContainerSpec) error {

	ctr := &spec.Container

	// read existing state if exists
	err := Read(ctr)
	if err != nil && err != NotFound {
		return ErrorE("failed to read container entry", err)
	}

	// create network namespace entry
	netns := &NetNS{
		Name: ctr.Namespace,
	}
	// read existing state if exists
	err = Read(netns)
	if err != nil && err != NotFound {
		return ErrorE("failed to read namespace entry", err)
	}

	// add container to the namespace
	netns.Containers = append(netns.Containers, ctr.Key())

	objs := []Object{ctr, netns}

	// save the container and namespace objects to db
	err = WriteObjects(objs)
	if err != nil {
		return ErrorE("failed to save container state", err)
	}

	return nil

}

func WithCogCNI(netns string) oci.SpecOpts {

	return func(
		ctx context.Context, client oci.Client, ctr *containers.Container,
		spec *oci.Spec,
	) error {

		// give container access to the network namespace for this materialization

		for i, x := range spec.Linux.Namespaces {
			if x.Type == "network" {
				spec.Linux.Namespaces[i] = specs.LinuxNamespace{
					Type: "network",
					Path: fmt.Sprintf("/var/run/netns/%s", netns),
				}
			}

		}

		return nil

	}

}

// Container Destruction ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func DeleteContainer(name, namespace string) error {

	ctx := namespaces.WithNamespace(context.Background(), namespace)
	container, err := getContainer(namespace, name)
	if err != nil {
		if err.Error() == "not found" {
			return nil
		}
		return ErrorE("failed to get container", err)
	}

	err = container.Delete(ctx, containerd.WithSnapshotCleanup)
	if err != nil {
		return ErrorE("failed to delete container", err)
	}

	return nil

}

func DeleteContainerTask(name, namespace string) error {

	fields := log.Fields{
		"namespace": namespace,
		"name":      name,
	}

	ctx := namespaces.WithNamespace(context.Background(), namespace)
	container, err := getContainer(namespace, name)
	if err != nil {
		if err.Error() == "not found" {
			return nil
		}
		return ErrorEF("failed to get container", err, fields)
	}

	task, err := container.Task(ctx, nil)
	if err != nil {
		if strings.HasPrefix(err.Error(), "no running task found") {
			return nil
		}
		return ErrorEF("could not get task", err, fields)
	}

	exit, err := task.Wait(ctx)
	if err != nil {
		return ErrorEF("failed to wait on task", err, fields)
	}

	err = task.Kill(ctx, syscall.SIGKILL)
	if err != nil {
		log.WithError(err).WithFields(fields).Warn("failed to kill task")
	}

	status := <-exit
	code, _, err := status.Result()
	if err != nil {
		fields["code"] = code
		log.WithError(err).WithFields(fields).Error("task exit failed")
	}

	_, err = task.Delete(ctx)
	if err != nil {
		return ErrorEF("failed to delete task", err, fields)
	}

	return nil

}

func DeleteContainerIfx(namespace, name string) error {

	fields := log.Fields{
		"ifx":       name,
		"namespace": namespace,
	}

	ctx, err := rtnl.OpenContext(namespace)
	if err != nil {
		return ErrorE("failed to open namepsace context", err)
	}
	defer ctx.Close()

	veth, err := rtnl.GetLink(ctx, name)
	if err != nil {
		if rtnl.IsNotFound(err) {
			return nil
		}
		return ErrorEF("failed to get container interface", err, fields)
	}

	err = veth.Del(ctx)
	if err != nil {
		return ErrorEF("failed to delete container interface", err, fields)
	}

	return nil

}

func DeleteContainerRecord(name, namespace string, a *Action) error {

	ctr := &Container{
		Namespace: namespace,
		Name:      name,
	}
	err := Read(ctr)
	if err != nil && err != NotFound {
		return ErrorEF("failed to fetch container record", err, log.Fields{
			"service": namespace,
			"name":    name,
		})
	}

	// remove ctr from namespace
	netns := &NetNS{Name: namespace}
	err = Read(netns)
	if err != nil && err != NotFound {
		return ErrorE("failed to fetch namespace record", err)
	}

	// delete the database entry and remove the container from the namespace
	netns.RemoveContainer(ctr.Key())

	tx := ObjectTx{
		Put:    []Object{netns},
		Delete: []Object{ctr},
	}

	err = RunObjectTx(tx)
	if err != nil {
		return ErrorE("failed to delete container from db", err)
	}

	return nil

}

func DestroyNetNS(netns *NetNS) error {

	netnsPath := fmt.Sprintf("/var/run/netns/%s", netns.Name)
	err := unix.Unmount(netnsPath, unix.MNT_DETACH)
	if err != nil {
		if err.Error() == "no such file or directory" {
			return nil
		}
		return ErrorE("failed to unmount netns", err)
	}
	err = unix.Unlink(netnsPath)
	if err != nil {
		return ErrorE("failed to unlink netns", err)
	}

	err = Delete(netns)
	if err != nil {
		return ErrorE("failed to delete netns db object", err)
	}

	return nil

}

// Helpers
// ============================================================================

var __client *containerd.Client

var (
	svcbrAddr = flag.String(
		"svcip", "172.31.0.1", "control IP for service bridge, assumed /16")
	svcPrefix = net.IPv4Mask(255, 255, 0, 0)
	svcIP     net.IP
)

func initContainers() {

	svcIP = net.ParseIP(*svcbrAddr)
	if svcIP == nil {
		log.WithFields(log.Fields{"ip": *svcbrAddr}).Fatal("invalid service bridge IP")
	}

	initSvcbr()

}

func initSvcbr() {

	ctx := &rtnl.Context{}

	// ensure the svcbr exists
	svcbr := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name:   "svcbr",
			Bridge: &rtnl.Bridge{},
		},
	}
	err := svcbr.Present(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure svcbr is present")
	}

	addr := rtnl.NewAddress()
	addr.Info.Address = &net.IPNet{
		IP:   svcIP,
		Mask: svcPrefix,
	}
	err = svcbr.AddAddr(ctx, addr)
	if err != nil && err.Error() != "file exists" {
		log.WithError(err).Fatal("faled to add ip to svcbr")
	}

	if !svcbr.Info.Promisc {

		err = svcbr.Down(ctx)
		if err != nil {
			log.WithError(err).Fatal("failed to bring interface down to put in to promisc")
		}

		err = svcbr.Promisc(ctx, true)
		if err != nil {
			log.WithError(err).Fatal("failed to bring svcbr into promisc mode")
		}

	}

	err = svcbr.Up(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to bring svcbr up")
	}

}

func containerdClient() (*containerd.Client, error) {

	if __client == nil {

		var err error
		__client, err = containerd.New("/run/containerd/containerd.sock")
		if err != nil {
			log.
				WithError(err).
				Error("failed to create containerd client")
			return nil, err
		}

	}

	return __client, nil

}

func getContainer(service, name string) (containerd.Container, error) {

	fields := log.Fields{
		"service": service,
		"op":      "get-container",
		"name":    name,
	}

	client, err := containerdClient()
	if err != nil {
		return nil, err
	}

	ctx := namespaces.WithNamespace(context.Background(), service)
	containers, err := client.Containers(ctx)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to list containers")
		return nil, err
	}

	for _, c := range containers {
		if name == c.ID() {
			return c, nil
		}
	}

	return nil, NotFound
}

func destroyVethPair(name string, vni int) error {

	ctx := &rtnl.Context{}

	// deleting one side of the pair deletes them both
	veth, err := rtnl.GetLink(ctx, name)
	if err != nil {

		// nothing to do
		if rtnl.IsNotFound(err) {
			return nil
		}

		return ErrorE("failed to get veth", err)
	}

	err = veth.Del(ctx)
	if err != nil {
		return ErrorE("failed to delete veth", err)
	}

	return nil
}

// a shameless ripoff of the iproute2 `ip netns add` command
//
// https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/tree/ip/ipnetns.c?h=v4.19.0#n635
//
// with the notable exception that we do not consider the namespace already
// existing to be a failure. if it already exists we have nothing to do and move
// on with our lives
func ensureMznNetworkNamespace(nsname string) (*NetNS, error) {

	fields := log.Fields{"namespace": nsname}

	netns := &NetNS{
		Name: nsname,
	}
	err := Read(netns)
	if err != nil && err != NotFound {
		return nil, ErrorEF("failed to read namespace entry", err, fields)
	}

	netnsRunDir := "/var/run/netns"

	// ensure base netns dir exists
	err = os.MkdirAll(netnsRunDir, 0755)
	if err != nil {
		return nil, ErrorEF("failed to ensure base netns dir", err, fields)
	}

	netnsPath := fmt.Sprintf("/var/run/netns/%s", nsname)
	madeMount := false

	// if the netowrk namespace does not exist create it
	if _, err := os.Stat(netnsPath); err != nil {

		runtime.LockOSThread()
		// go back to the default namespace when finished, if we can get back, panic,
		// we're no good stuck inside a mzn network namespace
		defer setDefaultNetNs()
		defer runtime.UnlockOSThread()

		// black magic
		for {

			err := unix.Mount("", netnsRunDir, "none", unix.MS_SHARED|unix.MS_REC, "")
			if err != nil {
				errno, ok := err.(syscall.Errno)
				if !ok || errno != unix.EINVAL || madeMount {
					return nil, ErrorE("netns empty bind mount failed", err)
				}

				err := unix.Mount(netnsRunDir, netnsRunDir, "none",
					unix.MS_BIND|unix.MS_REC, "")
				if err != nil {
					log.Error("netns self bind mount failed")
				}

				madeMount = true
			} else {
				break
			}

		}

		// create the /var/run/netns entry
		ns, err := os.OpenFile(netnsPath, os.O_RDONLY|os.O_CREATE, 0)
		if err != nil {
			return nil, ErrorEF("failed to create netns file", err, fields)
		}
		ns.Close()

		// unshare the current namespace
		err = unix.Unshare(unix.CLONE_NEWNET)
		if err != nil {
			os.RemoveAll(netnsPath)
			return nil, ErrorEF("failed to unshare NEWNET", err, fields)
		}

		//nspath := "/proc/self/ns/net"
		nspath := fmt.Sprintf("/proc/%d/task/%d/ns/net", os.Getpid(), unix.Gettid())
		err = unix.Mount(nspath, netnsPath, "none", unix.MS_BIND, "")
		if err != nil {
			os.RemoveAll(netnsPath)
			return nil, ErrorEF("failed to bindmount netns", err, fields)
		}

	}

	return netns, nil

}

func destroyMznNetworkNamespace(nsname string) error {

	nspath := fmt.Sprintf("/var/run/netns/%s", nsname)
	unix.Unmount(nspath, unix.MNT_DETACH)
	return unix.Unlink(nspath)

}

func setupLo(ctx *rtnl.Context) error {

	lo := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: "lo",
			Ns:   uint32(ctx.Fd()),
		},
	}
	err := lo.Read(ctx)
	if err != nil {
		log.WithError(err).Error("failed to get loopback device")
		return ErrorE("read lo", err)
	}

	addr, err := rtnl.ParseAddr("127.0.0.1/8")
	if err != nil {
		return ErrorE("parse localhost", err)
	}

	err = lo.AddAddr(ctx, addr)
	if err != nil && err.Error() != "file exists" {
		return ErrorE("add lo", err)
	}

	err = lo.Up(ctx)
	if err != nil {
		return ErrorE("lo up", err)
	}

	return nil

}

func getVethFromNs(name, ns string) (*rtnl.Link, error) {

	ctx, err := rtnl.OpenContext(ns)
	if err != nil {
		return nil, ErrorE("failed to open namespace", err)
	}
	defer ctx.Close()
	return rtnl.GetLink(ctx, name)

}

func setDefaultNetNs() {

	err := setNsFromPath("/proc/1/ns/net")
	if err != nil {
		log.Error(err)
		log.Fatal("default namespace jump failed, exiting")
	}

}

func setNsFromPath(path string) error {

	log.Infof("netns: entering %s", path)

	fields := log.Fields{"path": path}

	ns, err := os.Open(path)
	if err != nil {
		return ErrorEF("failed to open netns", err, fields)
	}
	defer ns.Close()

	err = unix.Setns(int(ns.Fd()), unix.CLONE_NEWNET)
	if err != nil {
		return ErrorEF("failed to jump into netns", err, fields)
	}

	return nil

}

// helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (ctr *ContainerSpec) Env() []string {

	var result []string
	for k, v := range ctr.Environment {
		result = append(result, fmt.Sprintf("%s=%s", k, v))
	}
	return result

}
