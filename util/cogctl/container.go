package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/dcomptb/cogs/pkg"
)

func containerCmds(delete, list *cobra.Command) {

	listContainers := &cobra.Command{
		Use:   "containers",
		Short: "list containers",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listContainers()
		},
	}
	list.AddCommand(listContainers)

	listNamespaces := &cobra.Command{
		Use:   "namespaces",
		Short: "list namespaces",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listNamespaces()
		},
	}
	list.AddCommand(listNamespaces)

	deleteContainer := &cobra.Command{
		Use:   "container <mzid> <name>",
		Short: "delete a container",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			deleteContainer(args[0], args[1])
		},
	}
	delete.AddCommand(deleteContainer)

}

func listContainers() {

	// we only have nex containers at the moment
	containers, err := cogs.ListContainers("nex")
	if err != nil {
		log.Fatal(err)
	}

	for _, c := range containers {

		log.Print(c.ID())

	}

}

func listNamespaces() {

	list, err := cogs.ListNamespaces()
	if err != nil {
		log.Fatal(err)
	}

	for _, ns := range list {

		log.Print(ns)

	}

}

func deleteContainer(mzid, name string) {

	task := &cogs.Task{
		Stages: []*cogs.Stage{
			{
				Actions: []*cogs.Action{
					{
						Kind:   "container",
						Action: cogs.DeleteCtrTask,
						Mzid:   mzid,
						Data: cogs.DeleteContainerData{
							Name:      name,
							Namespace: mzid,
						},
					},
				},
			},
			{
				Actions: []*cogs.Action{{
					Kind:   "container",
					Action: cogs.DeleteCtr,
					Mzid:   mzid,
					Data: cogs.DeleteContainerData{
						Name:      name,
						Namespace: mzid,
					},
				}},
			},
			{
				Actions: []*cogs.Action{{
					Kind:   "container",
					Action: cogs.DeleteCtrRecord,
					Mzid:   mzid,
					Data: cogs.DeleteContainerData{
						Name:      name,
						Namespace: mzid,
					},
				}},
			},
		},
	}

	err := cogs.LaunchTask(task)
	if err != nil {
		log.Printf("launch task failure")
		log.Fatal(err)
	}

}
