package main

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/dcomptb/cogs/pkg"
)

func cogCmds(list *cobra.Command) {

	listCoglets := &cobra.Command{
		Use:   "coglets",
		Short: "list coglets",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listCogs()
		},
	}
	list.AddCommand(listCoglets)

}

func listCogs() {

	coglets, err := cogs.ListCoglets()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, "cog\tid\thost\tpid\n")
	for _, c := range coglets {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%d\n", c.Cog, c.Id[:8], c.Host, c.Pid)
	}
	tw.Flush()

}
