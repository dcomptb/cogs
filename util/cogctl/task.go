package main

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/dcomptb/cogs/pkg"
)

func taskCmds(root, create, delete, list *cobra.Command) {

	var all bool
	listTasks := &cobra.Command{
		Use:   "tasks",
		Short: "list tasks",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listTasks(all)
		},
	}
	listTasks.Flags().BoolVarP(&all, "all", "a", false, "list all tasks")
	list.AddCommand(listTasks)

	launchTasks := &cobra.Command{
		Use:   "task [task.yaml]",
		Short: "create  tasks from a YAML spec",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			launchTasks(args[0])
		},
	}
	create.AddCommand(launchTasks)

	deleteTask := &cobra.Command{
		Use:   "tasks <taskid>",
		Short: "delete a task based on uuid prefix",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			cancelTask(args)
		},
	}
	delete.AddCommand(deleteTask)

	deleteErrors := &cobra.Command{
		Use:   "task-errors <taskid...>",
		Short: "clear errors on a task",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			clearTaskErrors(args)
		},
	}
	delete.AddCommand(deleteErrors)

	reset := &cobra.Command{
		Use:   "reset <taskid...>",
		Short: "reset a task",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			resetTask(args)
		},
	}
	root.AddCommand(reset)

	mask := &cobra.Command{
		Use:   "mask <taskid> <stage> <action>",
		Short: "mask an action so it's ignored by rex",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			stage, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}

			action, err := strconv.Atoi(args[2])
			if err != nil {
				log.Fatal(err)
			}

			maskAction(args[0], stage, action)
		},
	}
	root.AddCommand(mask)

}

func maskAction(taskid string, stage, action int) {

	t := &cogs.Task{Id: taskid}
	err := cogs.Read(t)
	if err != nil {
		log.Fatal(err)
	}

	t.MaskAction(stage, action)

	err = cogs.Write(t)
	if err != nil {
		log.Fatal(err)
	}

}

func listTasks(all bool) {

	tasks, err := cogs.ListTasks()
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(tasks, func(i, j int) bool {

		t0, _ := time.Parse(cogs.TimeFormat, tasks[i].Timestamp)
		t1, _ := time.Parse(cogs.TimeFormat, tasks[j].Timestamp)

		return t0.Before(t1)

	})

	fmt.Fprintf(tw, "time\tid\tstage\taction\tkind\tmzid\tinstance\taction\tdeps\tcomplete\terror\tmasked\n")
	for _, t := range tasks {
		for i, s := range t.Stages {
			for j, a := range s.Actions {

				if a == nil {
					continue
				}

				errmsg := ""
				if a.Error != nil {
					errmsg = *a.Error
				}

				if a.Complete && !all {
					continue
				}

				fmt.Fprintf(tw, "%s\t%s\t%d\t%d\t%s\t%s\t%s\t%s\t%v\t%v\t%s\t%v\n",
					t.Timestamp, t.Id, i, j, a.Kind, a.Mzid, a.MzInstance, a.Action, t.Deps, a.Complete, errmsg, a.Masked)

			}
		}
	}
	tw.Flush()

}

func launchTasks(file string) {

	task, err := cogs.ReadTask(file)
	if err != nil {
		log.Printf("read task failure")
		log.Fatal(err)
	}

	err = cogs.LaunchTask(task)
	if err != nil {
		log.Printf("launch task failure")
		log.Fatal(err)
	}

}

func cancelTask(prefixes []string) {

	for _, prefix := range prefixes {
		err := cogs.CancelTask(prefix)
		if err != nil {
			log.Printf("cancel task failure")
			log.Fatal(err)
		}
	}

}

func clearTaskErrors(tasks []string) {

	for _, task := range tasks {

		t := &cogs.Task{Id: task}
		err := cogs.Read(t)
		if err != nil {
			log.Fatal(err)
		}

		t.ClearErrors()
		err = cogs.Write(t)
		if err != nil {
			log.Fatal(err)
		}

	}

}

func resetTask(tasks []string) {

	for _, task := range tasks {

		t := &cogs.Task{Id: task}
		err := cogs.Read(t)
		if err != nil {
			log.Fatal(err)
		}

		t.Reset()
		err = cogs.Write(t)
		if err != nil {
			log.Fatal(err)
		}

	}

}
