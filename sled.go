package cogs

import "gitlab.com/mergetb/tech/sled/pkg"

func sledImageAction(mzinfo *MzInfo, nodeImages []*sled.Image) *Action {
	return &Action{
		Kind:   SledKind,
		Mzid:   mzinfo.Mzid,
		Action: ImageActionSled,
		Data:   nodeImages,
	}
}

func sledWriteAction(mzinfo *MzInfo, nodeImages []*sled.Image) *Action {
	return &Action{
		Kind:   SledKind,
		Mzid:   mzinfo.Mzid,
		Action: WriteActionSled,
		Data:   nodeImages,
	}
}

func sledWipeAction(mzinfo *MzInfo, nodeImages []*sled.Image) *Action {
	return &Action{
		Kind:   SledKind,
		Mzid:   mzinfo.Mzid,
		Action: WipeActionSled,
		Data:   nodeImages,
	}
}

func sledKexecAction(mzinfo *MzInfo, nodeImages []*sled.Image) *Action {
	return &Action{
		Kind:   SledKind,
		Mzid:   mzinfo.Mzid,
		Action: KexecActionSled,
		Data:   nodeImages,
	}
}
