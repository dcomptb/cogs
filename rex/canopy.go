package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/canopy/lib"
	"google.golang.org/grpc/status"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/dcomptb/cogs/fabric"
	"gitlab.com/dcomptb/cogs/pkg"
)

const (
	VTEP_MTU = 9166
	PHYS_MTU = 9216
)

func doCanopyAction(action *cogs.Action) error {

	log.WithFields(log.Fields{"action": action.Action}).Info("begin canopy action")

	var err error

	switch action.Action {

	case cogs.SetCanopyLinks:
		var spec *cogs.LinkSpec
		spec, err = decodeLinkSpec(action)
		if err == nil {
			err = doSetLink(*spec)
		}

	case cogs.RemoveCanopyLinks:
		var spec *cogs.LinkSpec
		spec, err = decodeLinkSpec(action)
		if err == nil {
			err = doRemoveLink(action.Mzid, *spec)
		}

	case cogs.SetCanopyServiceVtep:
		var spec *cogs.VtepSpec
		spec, err = decodeVtepSpec(action)
		if err == nil {
			err = doSetServiceVtep(action.Mzid, *spec)
		}

	case cogs.RemoveCanopyServiceVtep:
		var spec *cogs.VtepSpec
		spec, err = decodeVtepSpec(action)
		if err == nil {
			err = doRemoveServiceVtep(action.Mzid, *spec)
		}

	default:
		err = fmt.Errorf("unknown canopy task '%s'", action.Action)
	}

	if err == nil {
		action.Complete = true
		return nil
	} else {
		s, ok := status.FromError(err)
		if ok {
			err = fmt.Errorf(s.Message())
		}
		log.WithError(err).Error("canopy task failed")
		return err
	}

}

func doSetLink(spec cogs.LinkSpec) error {

	lp, err := fabric.NewLinkPlan(spec)
	if err != nil {
		return err
	}

	log.Debugf("\n%s", lp.String())
	cogs.Write(lp)

	return lp.Create().Exec()

}

func doRemoveLink(mzid string, spec cogs.LinkSpec) error {

	lp, err := fabric.NewLinkPlan(spec)
	if err != nil {
		return err
	}

	log.Debugf("\n%s", lp.String())

	return lp.Destroy().Exec()

}

func doSetServiceVtep(mzid string, spec cogs.VtepSpec) error {

	fields := log.Fields{
		"mzid": mzid,
		"vni":  spec.Vni,
	}

	log.WithFields(fields).Info("setting up vteps")

	cc := canopy.NewClient()
	cfg := common.GetConfig()

	for _, v := range spec.Vteps {

		fields["node"] = v.Node
		fields["dev"] = v.Dev
		fields["bridge"] = v.Bridge
		fields["tunnelip"] = v.TunnelIP

		log.WithFields(fields).Info("vtep")

		vtepName := fmt.Sprintf("vtep%d", spec.Vni)

		cc.SetVtep(
			&canopy.VtepPresence{
				Presence: canopy.Presence_Present,
				Vni:      int32(spec.Vni),
				Parent:   v.Dev,
				TunnelIp: v.TunnelIP,
			},
			vtepName, v.Node,
		)
		cc.SetMaster(
			&canopy.Master{
				Presence: canopy.Presence_Present,
				Value:    v.Bridge,
			},
			vtepName, v.Node,
		)
		cc.SetAccessPort(
			&canopy.Access{
				Presence: canopy.Presence_Present,
				Vid:      int32(spec.Vid),
			},
			vtepName, v.Node,
		)
		cc.SetMtu(int32(cfg.Net.VtepMtu), vtepName, v.Node)
		cc.SetLink(
			canopy.UpDown_Up,
			vtepName, v.Node,
		)

	}

	err := cc.Commit()
	if err != nil {
		log.WithError(err).Error("service vtep commit failed")
		return err
	}

	return nil

}

func doRemoveServiceVtep(mzid string, spec cogs.VtepSpec) error {

	fields := log.Fields{
		"mzid":  mzid,
		"vni":   spec.Vni,
		"count": len(spec.Vteps),
	}

	log.WithFields(fields).Info("removing up vteps")

	cc := canopy.NewClient()

	for _, v := range spec.Vteps {

		fields["node"] = v.Node
		fields["dev"] = v.Dev
		fields["bridge"] = v.Bridge

		log.WithFields(fields).Info("removing vtep")

		vtepName := fmt.Sprintf("vtep%d", spec.Vni)

		cc.SetVtep(
			&canopy.VtepPresence{
				Presence: canopy.Presence_Absent,
				Vni:      int32(spec.Vni),
				Parent:   v.Dev,
			},
			vtepName, v.Node,
		)

	}

	err := cc.Commit()
	if err != nil {
		log.WithError(err).Error("service vtep commit failed")
		return err
	}

	return nil

}

func decodeLinkSpec(action *cogs.Action) (*cogs.LinkSpec, error) {

	spec := &cogs.LinkSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "canopy"}).
			Error("failed to parse LinkSpec")
		return nil, err
	}
	return spec, nil

}

func decodeVtepSpec(action *cogs.Action) (*cogs.VtepSpec, error) {

	spec := &cogs.VtepSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "canopy"}).
			Error("failed to parse VtepSpec")
		return nil, err
	}
	return spec, nil

}

func setHarborInfralink(spec *cogs.NodeSpec) error {

	return doSetLink(cogs.LinkSpec{
		Kind: cogs.InfraLink,
		Vni:  cogs.HarborVni,
		Vid:  cogs.HarborVid,
		Endpoints: []cogs.Endpoint{{
			Node:      spec.Resource,
			Interface: spec.Interface,
		}},
	})

}

func setInfralink(spec *cogs.NodeSpec) error {

	return doSetLink(cogs.LinkSpec{
		Kind: cogs.InfraLink,
		Vni:  spec.Infranet.Vni,
		Vid:  spec.Infranet.Vid,
		Endpoints: []cogs.Endpoint{{
			Node:      spec.Resource,
			Interface: spec.Interface,
		}},
	})

}
