package main

import (
	"context"
	"flag"
	"fmt"
	"time"

	"github.com/containerd/containerd/containers"
	"github.com/containerd/containerd/oci"
	"github.com/mitchellh/mapstructure"
	"github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/nex/pkg"
	"google.golang.org/grpc"

	"gitlab.com/dcomptb/cogs/pkg"
)

var nexAddr = flag.String("nex", "127.0.0.1:6000", "nex address")

func doNexAction(action *cogs.Action) error {

	switch action.Action {

	case cogs.CreateNexNetwork:
		return doCreateNexNetwork(action)

	case cogs.AddNexMembers:
		return doAddNexMembers(action)

	default:
		return fmt.Errorf("unknown nex action '%s'", action.Action)

	}

}

func doCreateNexNetwork(action *cogs.Action) error {

	spec := &cogs.NexNetworkSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "nex"}).
			Error("failed to parse nex network")
		return err
	}

	err = validateSpec(spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "nex"}).
			Error("bad spec")
		return err
	}

	return withNexClient(spec.Endpoint, func(client nex.NexClient) error {

		// clear out any existing network
		client.DeleteNetwork(context.TODO(),
			&nex.DeleteNetworkRequest{Name: spec.Network.Name})

		_, err = client.AddNetwork(context.TODO(),
			&nex.AddNetworkRequest{Network: &spec.Network})
		if err != nil {
			return common.ErrorE("failed to create nex network", err)
		}

		action.Complete = true
		return nil

	})

}

func doAddNexMembers(action *cogs.Action) error {

	spec := &cogs.NexMemberSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorE("failed to decode nex task", err)
	}

	return withNexClient(spec.Endpoint, func(client nex.NexClient) error {

		spec.MemberList.Force = true
		_, err := client.AddMembers(context.TODO(), &spec.MemberList)
		if err != nil {
			return common.ErrorE("failed to add nex members", err)
		}

		action.Complete = true
		return nil

	})

}

func addNexMember(net string, spec *cogs.NodeSpec) error {

	return withNexClient(spec.SvcEndpoint, func(client nex.NexClient) error {

		_, err := client.AddMembers(context.TODO(), &nex.MemberList{
			Net:   net,
			Force: true,
			List: []*nex.Member{{
				Mac:  spec.Mac,
				Name: spec.Xname,
			}},
		})
		return err

	})

}

func dial(addr string) (*grpc.ClientConn, nex.NexClient) {

	endpoint := fmt.Sprintf("%s:6000", addr)
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	return conn, nex.NewNexClient(conn)
}

func withNexClient(addr string, f func(nex.NexClient) error) error {

	conn, cli := dial(addr)
	defer conn.Close()

	var err error
	for i := 0; i < 10; i++ {
		_, err = cli.GetNetworks(context.TODO(), &nex.GetNetworksRequest{})
		if err == nil {
			return f(cli)
		}
		time.Sleep(500 * time.Millisecond)
	}

	return common.ErrorE("failed to connect to nex", err)

}

func validateSpec(spec *cogs.NexNetworkSpec) error {

	if spec.Network.Name == "" {
		return fmt.Errorf("nex network name missing")
	}

	return nil

}

func withNexConfig(
	ctx context.Context, client oci.Client, c *containers.Container,
	spec *oci.Spec) error {

	spec.Mounts = append(spec.Mounts, specs.Mount{
		Destination: "/etc/nex/nex.yml",
		Source:      "/etc/cogs/nex.yml",
		Options:     []string{"rbind", "ro"},
		Type:        "bind",
	})

	return nil

}
