package main

import (
	"context"
	"flag"
	"fmt"
	"sync"
	"sync/atomic"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"

	. "gitlab.com/dcomptb/cogs/common"
	cog "gitlab.com/dcomptb/cogs/pkg"
)

var (
	etcd        *clientv3.Client
	BELUGA_ADDR string = "beluga.dcomptb.net"
)

func doTask(task *cog.Task) error {

	task.Init()

	//for _, stage := range task.Stages {
	for stage := task.Stages[0]; stage != nil; stage = stage.Next {

		// actions within a stage can be done in parallel, but stages themselves
		// must be serialized
		var wg sync.WaitGroup

		// atomic counter used by go routines to count errors
		var errorCount uint32

		for _, action := range stage.Actions {
			if action == nil {
				continue
			}
			action.SetTask(task)
			action.SetStage(stage)

			if action.Masked {
				continue
			}

			if !action.Complete {
				wg.Add(1)
				go func(a *cog.Action) {

					defer wg.Done()
					err := doAction(a)
					if err != nil {
						a.Error = new(string)
						*a.Error = err.Error()
						log.Error(err)
						atomic.AddUint32(&errorCount, 1)
					} else {
						a.Error = nil
					}
					cog.Write(task)

				}(action)
			}

		}

		wg.Wait()

		// bail if anything in the stage failed
		if errorCount > 0 {
			return fmt.Errorf("stage failed")
		}

	}

	return nil

}

func doAction(action *cog.Action) error {

	switch action.Kind {

	case cog.ContainerKind:
		return doContainerAction(action)

	case cog.NexKind:
		return doNexAction(action)

	case cog.CanopyKind:
		return doCanopyAction(action)

	case cog.PlumbingKind:
		return doPlumbingAction(action)

	case cog.BookkeepingKind:
		return doBookkeepingAction(action)

	case cog.NodeSetupKind:
		return nodeSetup(action)

	case cog.NodeRecycleKind:
		return nodeRecycle(action)

	default:
		return fmt.Errorf("unknown action kind %s", action.Kind)

	}

}

func main() {
	InitLogging("rex")
	log.Printf("rex %s", cog.Version)

	log.Printf("rex starting id=%s", cog.CogletId()[:8])

	flag.Parse()

	cog.Init()

	err := cog.EnsureEtcd(&etcd)
	if err != nil {
		log.Fatal(err)
	}
	log.Trace("connected to etcd")

	err, lease := cog.RegisterCoglet(etcd, "rex")
	if err != nil {
		log.Fatal(err)
	}
	log.Trace("registered coglet")

	for {
		etcd.KeepAliveOnce(context.TODO(), lease)
		cog.Exec(&etcd, "node", doTask)
	}
}
