package main

import (
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/dcomptb/cogs/pkg"
)

func nodeSetup(action *cogs.Action) error {

	fields := log.Fields{
		"mzid":   action.Mzid,
		"action": action.Action,
	}

	log.WithFields(fields).Info("begin node setup")

	spec := &cogs.NodeSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorEF("failed to decode node setup spec", err, fields)
	}

	// ensure all infralinks on harbor
	err = setHarborInfralink(spec)
	if err != nil {
		return common.ErrorEF("failed to set infralink", err, fields)
	}

	// set nex member name
	err = addNexMember(action.Mzid, spec)
	if err != nil {
		return common.ErrorEF("failed to add nex member", err, fields)
	}

	// setup foundry
	err = addFoundryConfig(spec)
	if err != nil {
		return common.ErrorEF("failed to add foundry config", err, fields)
	}

	// ensure node has juice
	err = belugaOn(spec.Resource, false)
	if err != nil {
		return common.ErrorEF("failed to ensure node power", err, fields)
	}

	// soft cycle the node
	err = belugaCycle(spec.Resource, true)
	if err != nil {
		return common.ErrorEF("failed to soft cycle node", err, fields)
	}

	// image the node
	err = sledImage(spec, false)
	if err != nil {
		return common.ErrorEF("failed to image node", err, fields)
	}

	// put the node on it's materialization network
	err = setInfralink(spec)
	if err != nil {
		return common.ErrorEF("failed to place node in mzn network", err, fields)
	}

	log.WithFields(fields).Info("node setup complete")

	action.Complete = true
	return nil

}

func nodeRecycle(action *cogs.Action) error {

	fields := log.Fields{
		"mzid":   action.Mzid,
		"action": action.Action,
	}

	log.WithFields(fields).Info("begin node recycle")

	spec := &cogs.NodeSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorEF("failed to decode node recycle spec", err, fields)
	}

	// put the node on the harbor network
	err = setHarborInfralink(spec)
	if err != nil {
		return common.ErrorEF("failed to set infralink", err, fields)
	}

	// ensure node has juice
	err = belugaOn(spec.Resource, false)
	if err != nil {
		return common.ErrorEF("failed to ensure node power", err, fields)
	}

	// soft cycle the node
	err = belugaCycle(spec.Resource, true)
	if err != nil {
		return common.ErrorEF("failed to soft cycle node", err, fields)
	}

	// image the node
	err = sledImage(spec, false)
	if err != nil {
		return common.ErrorEF("failed to image node", err, fields)
	}

	log.WithFields(fields).Info("node recycle complete")

	action.Complete = true
	return nil

}
