package common

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
)

func EtcdClient() (*clientv3.Client, error) {

	log.Trace("creating new client...")
	c := GetConfig()

	var tlsc *tls.Config
	if c.Etcd.Tls != nil {

		log.Trace("etcd tls enabled")

		log.WithFields(log.Fields{
			"cacert": c.Etcd.Tls.Cacert,
			"cert":   c.Etcd.Tls.Cert,
			"key":    c.Etcd.Tls.Key,
		}).Trace("tls config")

		capool := x509.NewCertPool()
		capem, err := ioutil.ReadFile(c.Etcd.Tls.Cacert)
		if err != nil {
			return nil, ErrorE("failed to read cacert", err)
		}
		ok := capool.AppendCertsFromPEM(capem)
		if !ok {
			return nil, ErrorF("capem is not ok", log.Fields{"ok": ok})
		}

		cert, err := tls.LoadX509KeyPair(
			c.Etcd.Tls.Cert,
			c.Etcd.Tls.Key,
		)
		if err != nil {
			return nil, ErrorE("failed to load cert/key pair", err)
		}

		tlsc = &tls.Config{
			RootCAs:      capool,
			Certificates: []tls.Certificate{cert},
		}
	} else {
		log.Trace("etcd tls disabled")
	}

	connstr := fmt.Sprintf("%s:%d", c.Etcd.Address, c.Etcd.Port)
	log.WithFields(log.Fields{
		"connstr": connstr,
	}).Trace("etcd connection string")

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{connstr},
		DialTimeout: 2 * time.Second,
		TLS:         tlsc,
	})

	log.Trace("client created")
	return cli, err

}
