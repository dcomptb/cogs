package common

import (
	"fmt"
	"io/ioutil"

	"github.com/mergetb/yaml/v3"
	log "github.com/sirupsen/logrus"
)

// singleton config, one instance per library, not modifiable outside library
// accessed through GetConfig()
var c *Config = nil

type TlsConfig struct {
	Cacert string
	Cert   string
	Key    string
}

type ServiceConfig struct {
	Address string
	Port    int
	Tls     *TlsConfig
}

func (s *ServiceConfig) Endpoint() string {
	return fmt.Sprintf("%s:%d", s.Address, s.Port)
}

type Tuning struct {
	TaskQuantum int `yaml:"taskQuantum"`
	TaskLease   int `yaml:"taskLease"`
	FailSleep   int `yaml:"failSleep"`
}

type NetConfig struct {
	VtepIfx         string `yaml:"vtepIfx"`
	Mtu             int    `yaml:"mtu"`
	VtepMtu         int    `yaml:"vtepMtu"`
	ServiceTunnelIP string `yaml:"serviceTunnelIP"`

	ExternalIfx     string `yaml:"externalIfx"`
	ExternalIP      string `yaml:"externalIP"`
	ExternalSubnet  string `yaml:"externalSubnet"`
	ExternalGateway string `yaml:"externalGateway"`
}

type Config struct {
	Driver    ServiceConfig
	Etcd      ServiceConfig
	Elastic   ServiceConfig
	Commander ServiceConfig
	Beluga    ServiceConfig
	Nex       ServiceConfig
	Tuning    Tuning
	Net       NetConfig
}

func GetConfig() Config {
	if c == nil {
		LoadConfig()
	}
	return *c
}

func LoadConfig() {

	data, err := ioutil.ReadFile("/etc/dcomp/cogs.yml")
	if err != nil {
		log.Fatal("could not read configuration file /etc/dcomp/cogs.yml %v", err)
	}

	cfg := &Config{}
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		log.Fatal("could not parse configuration file %v", err)
	}

	log.WithFields(log.Fields{
		"config": fmt.Sprintf("%+v", *cfg),
	}).Debug("config")

	c = cfg

}
