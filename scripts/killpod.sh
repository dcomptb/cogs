#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: killpod <mzid>"
  exit 1
fi

name=$1

ctr -n etcd task kill -s 9 $name
ctr -n foundry task kill -s 9 $name
ctr -n nex task kill -s 9 $name
ctr -n pxeboot task kill -s 9 $name
ctr -n sledb task kill -s 9 $name

ctr -n etcd c rm $name
ctr -n foundry c rm $name
ctr -n nex c rm $name
ctr -n pxeboot c rm $name
ctr -n sledb c rm $name

