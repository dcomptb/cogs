#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: listpod <mzid>"
  exit 1
fi

name=$1

ctr -n etcd c ls 
ctr -n foundry c ls 
ctr -n nex c ls 
ctr -n pxeboot c ls 
ctr -n sledb c ls 

