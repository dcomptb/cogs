module gitlab.com/dcomptb/cogs

go 1.12

require (
	github.com/containerd/containerd v1.2.2
	github.com/containerd/continuity v0.0.0-20190426062206-aaeac12a7ffc // indirect
	github.com/containerd/cri v1.11.1 // indirect
	github.com/containerd/fifo v0.0.0-20190226154929-a9fb20d87448 // indirect
	github.com/containerd/typeurl v0.0.0-20190507170917-b36cf1e3e273 // indirect
	github.com/coreos/etcd v3.3.13+incompatible
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-events v0.0.0-20170721190031-9461782956ad // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gogo/googleapis v1.2.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/mergetb/yaml v2.1.0+incompatible
	github.com/mergetb/yaml/v3 v3.0.1
	github.com/mitchellh/mapstructure v1.1.2
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/opencontainers/runtime-spec v1.0.1
	github.com/osrg/gobgp v2.0.0+incompatible
	github.com/sirupsen/logrus v1.4.1
	github.com/spf13/cobra v0.0.3
	github.com/syndtr/gocapability v0.0.0-20180916011248-d98352740cb2 // indirect
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	gitlab.com/dcomptb/minnowchassis v0.1.3 // indirect
	gitlab.com/mergetb/engine v0.0.0-20190311222821-51822dcf0339
	gitlab.com/mergetb/site v0.1.5
	gitlab.com/mergetb/tech/beluga v0.1.3 // indirect
	gitlab.com/mergetb/tech/canopy v0.2.6
	gitlab.com/mergetb/tech/foundry v0.1.8
	gitlab.com/mergetb/tech/nex v0.4.4
	gitlab.com/mergetb/tech/rtnl v0.1.6
	gitlab.com/mergetb/tech/sled v0.1.8-0.20190327080451-898fe3a26743
	gitlab.com/mergetb/tech/stor v0.1.1 // indirect
	gitlab.com/mergetb/xir v0.0.0-20190413174208-948f04196243
	golang.org/x/sys v0.0.0-20190412213103-97732733099d
	google.golang.org/grpc v1.20.1
)

replace github.com/docker/distribution v2.7.1+incompatible => github.com/docker/distribution v2.7.1-0.20190205005809-0d3efadf0154+incompatible

// replace gitlab.com/mergetb/xir v0.0.0-20190413064644-d7142e485cd3 => /home/ry/src/gitlab.com/mergetb/xir

// replace gitlab.com/mergetb/site v0.1.3 => /home/ry/src/gitlab.com/mergetb/site

// replace gitlab.com/mergetb/tech/rtnl v0.0.0-20190317004937-9e8b88bcbe76 => /home/ry/src/gitlab.com/mergetb/tech/rtnl

// replace gitlab.com/mergetb/tech/foundry v0.1.6 => /home/ry/src/gitlab.com/mergetb/tech/foundry

// replace gitlab.com/mergetb/tech/canopy v0.2.5 => /home/ry/src/gitlab.com/mergetb/tech/canopy

// replace gitlab.com/mergetb/tech/beluga v0.1.3 => /home/ry/src/gitlab.com/mergetb/tech/beluga
